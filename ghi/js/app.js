function createCard(name, description, pictureUrl, datestart, dateend, loc) {

    const startdate = new Date(datestart).toLocaleDateString('en-US')
    const enddate = new Date(dateend).toLocaleDateString('en-US')

    return `
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">

            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h6 class="card-subtitle mb-2 text-muted">${loc}</h6>
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            <h6 class="c
            </div>
            <div class= "card-footer">${startdate} - ${enddate}</div>
        </div>
        `;
    }

// function createError() {
//     return `
//     <div class= "alert alert-danger">
//     Bad request!
//     </div>
//     `
// }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alertDiv.classList.remove("d-none");
            alertDiv.classList.add("d-block");
            // const html2 = createError()
            // const main = document.querySelector('main')
            // main.innerHTML += html2

            // console.error
            // //console.log(response);
            // console.error('Houston we have a problem!');


        }   else {
            const data = await response.json();
            let count = 0

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const datestart = details.conference.starts;
                    const dateend = details.conference.ends;
                    const loc = details.conference.location.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, datestart, dateend, loc);
                    // console.log(html);

                    const column = document.querySelectorAll('.col');
                    column[count].innerHTML += html;
                    count += 1
                    if (count === column.length) {
                        count = 0
                    }

                }
            }

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();
            //     const descTag = document.querySelector('.card-text');
            //     descTag.innerHTML = details.conference.description;

            //     console.log(details);

            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;


            }

        }catch (e) {
            //console.log(data);
            // console.error('console data error', e);
            alertDiv.classList.remove("d-none");
            alertDiv.classList.add("d-block");
    }

});
